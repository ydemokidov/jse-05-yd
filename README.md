# Task Manager Application
## Developer Info
**Name:** Yuriy Demokidov  
**Company:** T1-Consulting  
**Email:** ydemokidov@t1-consulting.ru  
**Corp. Email:** t1-consulting@t1-consulting.ru  

## Hardware
**RAM:** 8GB  
**CPU:** Intel Core i7  
**SSD:** 512 GB  

## Software
**OS:** Windows 10  
**JDK:** 1.8.0_144  

## Build Program
```
mvn clean install
```

## Run program
```
java -jar ./task-manager.jar
```
